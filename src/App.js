import { Link } from "react-router-dom";

// import routes from "./routes";
import Routes from "./Routes";


function App() {
  return (
    <>
      <main>
        <Routes />
      </main>
    </>
  );
}

export default App;
