import { Link } from "react-router-dom";
import React from "react";



export default function PostList() {

    const [posts, updatePosts] = React.useState([]);



    React.useEffect(() => {
        fetch('/post.mock.json').then(response => response.json()).then(val => {
            updatePosts(val.posts)
        })
    }, []);


    return <div className="card">
        <h2>Posts</h2>
        <hr></hr>
        <table className="table">
            <thead className="header">
                <tr>
                    <th>Post</th>
                    <th>Created Date</th>
                    <th>Check Comments</th>
                </tr>
            </thead>
            <tbody className="body">
                {posts.map(val => {
                    return (
                        <tr key={val.id}>
                            <td className="post-text">{val.text}</td>
                            <td>{val.date}</td>
                            <td className="action">
                                <Link className="btn btn-primary" to={val.id}>Visit</Link>
                            </td>
                        </tr>
                    )
                })}


            </tbody>
        </table>
    </div>
}
