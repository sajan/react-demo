import { useParams, useNavigate } from "react-router";
import { useSearchParams } from "react-router-dom";
import { Link } from "react-router-dom";
import React from "react";

export default function Post() {
    const { postId } = useParams();
    const navigate = useNavigate();
    const [queryParams] = useSearchParams();


    const [post, updatePost] = React.useState(null);
    const [comments, updateComments] = React.useState([]);

    React.useEffect(() => {
        fetch('/post.mock.json').then(response => response.json()).then(val => {
            const post = val.posts.find(x => x.id === postId);
            console.log({ post });
            updatePost(post);
            if(post && post.comments) {
                updateComments(post.comments);
            }
        })
    }, []);


    function removeComment(index) {
        console.log({ index });
    }



    return (
        <div className="card">
            <Link to="/">Back</Link>
            <p>{post?.text}</p>
            <hr></hr>
            <table className="table">
                <thead className="header">
                    <tr>
                        <th>Comment</th>
                        <th>Comment Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody className="body">
                    {comments.map(comment => {
                        return (
                            <tr key={comment.id}>
                                <td className="post-text">{comment.text}</td>
                                <td>12 Dec, 2022</td>
                                <td className="action">
                                    <button className="btn btn-primary" >Edit</button>
                                    <button className="btn btn-danger" onClick={() => { if (window.confirm('Delete this comment?')) { removeComment(comment.id) }; }}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

        </div>
    );
}
