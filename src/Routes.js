import { Route, Routes as BaseRoutes } from "react-router-dom";
import Home from "./pages/Home";
import PostList from "./pages/PostList";
import Post from "./pages/Post";

export default function Routes() {
  return (
    <BaseRoutes>
      <Route path="/" element={<PostList />} />
      <Route path=":postId" element={<Post />} />
      {/* <Route path="post">
        <Route index element={<PostList />} />
        <Route path=":postId" element={<Post />} />
      </Route> */}
    </BaseRoutes>
  );
}
